:;; Personal setup file

;; Make sure desired packages are installed properly
(prelude-require-packages '(
                            nyan-mode
                            go-mode
;;                            sanityinc-color-theme-tomorrow
;;                            solarized-theme
                            minimal-theme
))

;; Set default theme
(setq prelude-theme 'sanityinc-color-theme-tomorrow)

;; Disable flyspell
(setq prelude-flyspell nil)

;; Setup directory for personal snippets
(setq yas-snippet-dirs
      '("~/.emacs.d/personal/snippets"
        ))

;; Set shortcut for company complete
(global-set-key (kbd "M-SPC") 'company-complete-common)

;; Remove preludes key chord definitions
(key-chord-define-global "jj" nil)
(key-chord-define-global "jk" nil)
(key-chord-define-global "jl" nil)
(key-chord-define-global "JJ" nil)
(key-chord-define-global "uu" nil)
(key-chord-define-global "xx" nil)
(key-chord-define-global "yy" nil)

;; Set custom key chords.
(key-chord-define-global "y;" 'imenu)
(key-chord-define-global ".," 'evil-ace-jump-line-mode)
(key-chord-define evil-insert-state-map "ei" 'evil-normal-state)

(define-key global-map (kbd "C-x SPC") 'ace-jump-mode-pop-mark)
